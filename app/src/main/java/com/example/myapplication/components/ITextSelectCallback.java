package com.example.myapplication.components;


interface ITextSelectCallback {
    void onTextUpdate(String updatedText);
}
