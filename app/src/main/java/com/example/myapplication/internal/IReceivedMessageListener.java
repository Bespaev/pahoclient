package com.example.myapplication.internal;

import com.example.myapplication.model.ReceivedMessage;

public interface IReceivedMessageListener {

    void onMessageReceived(ReceivedMessage message);
}